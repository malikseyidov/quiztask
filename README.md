# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* The repository is a small quiz web app by coding using by html, css, bootstrap, js and jquery on front-end side and express.js and mysql on back-end side.

### How do I get set up? ###

* First of all you should clone the repository from Bitbucket to your local by using (git clone https://malikseyidov@bitbucket.org/malikseyidov/quiztask.git) or (git clone git@bitbucket.org:malikseyidov/quiztask.git) if your computer has got access by SSH on terminal.
* Install node modules into directory of the app by using (npm install ) because of using node.js/express.js on backend.
* Import the .sql file to your mysql by using (mysql -u malikk -p2412 quiz < quiz_database.sql) .
* Check your node version with (node -v) and express.js with (npm view express version). if express.js doesnt exist install with (npm install express --save)
* Finally open the directory of app on terminal and RUN the app by using (node server.js) or if you installed the nodemon package, you can also use the (nodemon server.js) to RUN the app.


