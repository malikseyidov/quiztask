$(document).ready (function(){ 
    closeAlert();  
});


function openAlert(getData){

    if(getData.action===false){
        $("#danger-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#danger-alert").slideUp(500);
        });
    }else if (getData.action===true){
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
            window.location.replace("http://localhost:3000");
        });
    }
}

function closeAlert(){
    $("#danger-alert").hide();
    $("#success-alert").hide();
}

function select(){
    var lang=document.getElementById("sel1").value;
    localStorage.setItem("lang",lang);
    setTimeout(function () {
        location.reload()
    }, 10);
    
}