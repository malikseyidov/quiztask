function changepsw(){

    var o_password = document.getElementById("ppsw").value;
    var n_password = document.getElementById("npsw").value;
    var cn_password = document.getElementById("cnpsw").value;
    var token = localStorage.getItem("token");
    var logindata = {
        
        "old_password": o_password,
        "new_password": n_password,
        "cnew_password": cn_password,
        "token": token
    };
    

    xhr = new XMLHttpRequest();
    var url = "http://localhost:3000/changepassword";
    var that = this;
    xhr.open("POST", url, true);
    
    xhr.setRequestHeader("Content-type", "application/json");
    
    xhr.onreadystatechange = function (){

        if (xhr.readyState == 4 && xhr.status == 200){

            var qisalt = JSON.parse(xhr.responseText);
            that.closeAlert();
            that.openAlert(qisalt);


            if(qisalt.action){
                document.getElementById("dsss2").innerHTML = "Congrulations: " + qisalt.message;
                
            }else{
                document.getElementById("dsss").innerHTML = "Attention: " + qisalt.message;
            }
        
        }
        
    }
    
    if(o_password==""||n_password==""||cn_password==""){
        document.getElementById("error").innerHTML ="* Fill all fillings"
    }else if(n_password.length<4){
        document.getElementById("error").innerHTML ="* Password must contain at least 4 characters"
    }else if(n_password != cn_password){
        document.getElementById("error").innerHTML ="* The specified passwords do not match"
    }
    else{
        document.getElementById("error").innerHTML ="";
        xhr.send(JSON.stringify(logindata,));
    
    }
}