function showpsdsp() {
    var x = document.getElementById("ppsw");
    var y = document.getElementById("npsw");
    var z = document.getElementById("cnpsw");
    if (x.type === "password" && y.type === "password" && z.type === "password") {
      x.type = "text";
      y.type = "text";
      z.type = "text";
    } else {
      x.type = "password";
      y.type = "password";
      z.type = "password";
    }
}