function diller(lang){
  
    for (var key in lang) {
      if (lang.hasOwnProperty(key)) {
        
            if(key=="login" && !localStorage.getItem("token")){
                document.getElementById(key).innerHTML=lang[key];
            }
            else if(localStorage.getItem("token")&&key!="login" && key!="namepp"&& key!="emailpp"&& key!="x1"&& key!="x2"&& key!="x3"&& key!="x4"&& key!="x5"){
                document.getElementById(key).innerHTML=lang[key];
                document.getElementById(key).placeholder=lang[key];
            } 
            else if(!localStorage.getItem("token")&&key!="login" && key!="namepp"&& key!="emailpp"&& key!="x1"&& key!="x2"&& key!="x3"&& key!="x4"&& key!="x5"&& key!="drop_userinfo"&& key!="drop_changepsw"&& key!="drop_yourresult"&& key!="drop_sendquestion"&& key!="drop_logout"){
                document.getElementById(key).innerHTML=lang[key];
                document.getElementById(key).placeholder=lang[key];
            }
        }
    } 
}