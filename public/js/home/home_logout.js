function logout(){
    localStorage.removeItem("token");
    localStorage.removeItem("username");
    localStorage.removeItem("lastname");
    localStorage.removeItem("email");
    window.location.assign("http://localhost:3000/signin");
}