function resultusers(){

    var ll=resultlangdata[0];
    var numberOfQuestion=localStorage.getItem("numberofquestion");
    var numberOfCorrect = localStorage.getItem("numberofCorrect");
    var numberOfIncorrect = localStorage.getItem("numberofIncorrect");
    var numberOfUnanswered = localStorage.getItem("numberofUnchecked");
    var percentage = localStorage.getItem("percentage");
    var firstName = document.getElementById("nameuser").value;
    var lastName = document.getElementById("surnameuser").value;
    var email = document.getElementById("emailuser").value;

    if(firstName!=""||lastName!=""){
     
     document.getElementById("namep").innerHTML = "<span id='namepp'></span> " +firstName+" "+lastName;
    }else{
     document.getElementById("namep").innerHTML = "";
    }
    
    if(email.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)){
     document.getElementById("emailp").innerHTML = "<span id='emailpp'></span> ('" + email+"')";
    } else{
     document.getElementById("emailp").innerHTML = "";
    }


    for (var keys in ll) {
     if (ll.hasOwnProperty(keys)) {
        if(firstName!=""||lastName!=""){
                if(keys=="namepp"){
                    document.getElementById(keys).innerHTML=ll[keys];
                }
            }
            if(email.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)){
                if(keys=="emailpp"){
                    document.getElementById(keys).innerHTML=ll[keys];
                }
            }
        }
    }
           
    var resultusers = {

     "numberOfQuestion":numberOfQuestion,
      "numberOfCorrect":numberOfCorrect,
      "numberOfIncorrect":numberOfIncorrect,
      "numberOfUnanswered":numberOfUnanswered,
      "percentage":percentage,
      "firstName": firstName,
      "lastName": lastName,
      "email":email,
      "date":new Date().toString().substr(3,18),
      "deletedno":"0"
      

    };
    
    

    xhr = new XMLHttpRequest();
    var url = "http://localhost:3000/results";
    
    xhr.open("POST", url, true);
    
    xhr.setRequestHeader("Content-type", "application/json");
    
    xhr.onreadystatechange = function (){

        if (xhr.readyState == 4 && xhr.status == 200){
         var qisalt = JSON.parse(xhr.responseText);
        } 
    }
   
    xhr.send(JSON.stringify(resultusers,));

    document.getElementById("myNav").style.width = "100%";
}