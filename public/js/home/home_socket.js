var socket = io.connect('http://localhost:3000');
var tokenblocked = localStorage.getItem("token");
      
socket.on('news', function (data) {
    socket.emit('my other event', tokenblocked )
    if(data.hello[0].blockedno==1){
        localStorage.removeItem("token");
        setTimeout(function () {
            location.reload()
        }, 10);
    }
});