function quizYarad(responseData) {
      
  var datass = {  "quiz": []};

  for (var i = 0; i < responseData.length; i++){
    
    datass.quiz.push(
    {
        "question": responseData[i].question,
        "correctAnswer": responseData[i].correctAnswer,
        "answers": {
          "a": responseData[i].aOption,
          "b": responseData[i].bOption,
          "c": responseData[i].cOption,
          "d": responseData[i].dOption
        }
    })
      
  }
      
  myQuestions = datass.quiz;
  var output = [];

  myQuestions.forEach(function(currentQuestion, questionNumber)  {
   
    var answers = [];

    for (i in currentQuestion.answers) {
     
      answers.push(
        '<label>\
            <input  type="radio" name="question'+questionNumber+'" value="'+i+'">\
          '+i+' :\
          '+currentQuestion.answers[i]+'\
        </label>'
      );
    }

    

    output.push(
      `<div class="panel panel-default"> 
      <div class="question panel-heading" ><h2> ${currentQuestion.question} </h2></div>
      <div class="answers panel-body"> ${answers.join("<br>")} </div>
      </div>`
     
    );
  });

  
  quizContainer.innerHTML = output.join("<br>");
}


function neticeGoster() { 

  window.scrollTo(0, 10000000);
  var answerContainers = quizContainer.querySelectorAll(".answers");

  var numCorrect = 0;
  var numNotCheck= 0;
  
  myQuestions.forEach(function(currentQuestion, questionNumber) {
    
    var answerContainer = answerContainers[questionNumber];
    var selector = `input[name=question${questionNumber}]:checked`;
    var userAnswer = (answerContainer.querySelector(selector) || {}).value;
    
    // console.log(userAnswer);


    answerContainers[questionNumber].style.color = "black"; 

    if (userAnswer === currentQuestion.correctAnswer) {
     
      numCorrect++; 
      

    }else if(userAnswer==undefined){
      numNotCheck++;
      
    }
  });
  var NofIc=myQuestions.length-numCorrect-numNotCheck;
  var perc=Math.floor((numCorrect/myQuestions.length ) * 100);
  localStorage.setItem("numberofquestion",myQuestions.length);
  localStorage.setItem("numberofCorrect",numCorrect);
  localStorage.setItem("numberofUnchecked",numNotCheck);
  localStorage.setItem("numberofIncorrect",NofIc);
  localStorage.setItem("percentage",perc);

}
function goster(){
  
  var answerContainers = quizContainer.querySelectorAll(".answers");

 
  
  myQuestions.forEach(function(currentQuestion, questionNumber) {
    
    var answerContainer = answerContainers[questionNumber];
    var selector = `input[name=question${questionNumber}]:checked`;
    var userAnswer = (answerContainer.querySelector(selector) || {}).value;

    answerContainers[questionNumber].style.color = "black"; 

    if (userAnswer === currentQuestion.correctAnswer) {
     
      (answerContainer.querySelector(selector)).parentNode.style.color = "green";

    } else {
      
      if(userAnswer !== undefined){

        (answerContainer.querySelector(selector)).parentNode.style.color = "red";
        
      }else{
        answerContainers[questionNumber].style.color = "gray"; 
      }
       
    }
  });
  var NofQ=localStorage.getItem("numberofquestion");
  var NofC=localStorage.getItem("numberofCorrect");
  var NofU=localStorage.getItem("numberofUnchecked");
  var NofIc=localStorage.getItem("numberofIncorrect");
  var perc=localStorage.getItem("percentage");
  
  resultsContainer.innerHTML = `<div class="container-fluid">

  <div style="margin-left:-15px; margin-right:-15px">  
  <span id="x1"></span> <strong style="text-decoration:underline">${NofC}</strong> <span id="x2"></span> <strong style="text-decoration:underline">${NofIc}</strong> <span id="x3"></span> <strong style="text-decoration:underline">${NofU}</strong> <span id="x4"></span> <strong style="text-decoration:underline">${NofQ}</strong> <span id="x5"></span> <strong style="text-decoration:underline">${perc}</strong>%
  </div>
</div>`;
   var ll=resultlangdata[0];
    for (var keys in ll) {
      if (ll.hasOwnProperty(keys)) {
      
        if(keys=="x1"||keys=="x2"||keys=="x3"||keys=="x4"||keys=="x5"){
          document.getElementById(keys).innerHTML=ll[keys];
        }
      }
    }
}

var quizContainer = document.getElementById("quiz");
var resultsContainer = document.getElementById("results");
var submitButton = document.getElementById("submit");
var show_result = document.getElementById("show_result");

show_result.addEventListener("click", goster);
submitButton.addEventListener("click", neticeGoster);
