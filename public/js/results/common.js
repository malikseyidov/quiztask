$(document).ready(function(){

// send READ query to Database and get the datas
var logindata = { 
    "email": localStorage.getItem("email")
};

function show(){
    xhr = new XMLHttpRequest();
    var url = "http://localhost:3000/userresult";
    var aar = this;
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = function (){

        if (xhr.readyState == 4 && xhr.status == 200){
            var goster = JSON.parse(xhr.responseText); 
            aar.modifyshow(goster);
        }
    }

    xhr.send(JSON.stringify(logindata,));
}
show();


    // Read the datas from database
    modifyshow = function (responseData){ 

        var datass = {  "quiz": []};
        for (var i = 0; i < responseData.length; i++){

            datass.quiz.push(
            {   "date":responseData[i].date,
                "firstName": responseData[i].firstname,
                "lastName": responseData[i].lastname,
                "email":responseData[i].email,
                "numberofquestion":responseData[i].numberOfQuestion,
                "numberofcorrectanswer":responseData[i].numberOfCorrect,
                "numberofincorrectanswer":responseData[i].numberOfIncorrect,
                "numberofunanswered":responseData[i].numberOfUnanswered,
                "percentage":responseData[i].percentage,
                "id":responseData[i].id
            })
            
        }
        myQuestions = datass.quiz;
        // Loop through data.report instead of data
        for (var f = 0; f < myQuestions.length; f++){
            var tr = $('<tr/>');
            
            // Indexing into data.report for each td element
            $(tr).append("<td>" + myQuestions[f].date + "</td>");
            $(tr).append("<td>" + myQuestions[f].firstName + "</td>");
            $(tr).append("<td>" + myQuestions[f].lastName + "</td>");
            $(tr).append("<td>" + myQuestions[f].email + "</td>");
            $(tr).append("<td>" + myQuestions[f].numberofquestion + "</td>");
            $(tr).append("<td>" + myQuestions[f].numberofcorrectanswer + "</td>");
            $(tr).append("<td>" + myQuestions[f].numberofincorrectanswer + "</td>");
            $(tr).append("<td>" + myQuestions[f].numberofunanswered + "</td>");
            $(tr).append("<td>" + myQuestions[f].percentage + "</td>");
            $(tr).append("<td>" + `
            <div class="btn_divv">
            <div class="dropdown">
                <button 
                id="drop${f}" 
                class="drop btn2 btn btn-success glyphicon glyphicon-chevron-down"  
                onclick="responsivedrop(
                    ${myQuestions[f].numberofquestion},
                    ${myQuestions[f].numberofcorrectanswer},
                    ${myQuestions[f].numberofincorrectanswer},
                    ${myQuestions[f].numberofunanswered},
                    ${myQuestions[f].percentage},
                    ${f}
                )">
                </button>
                <div id="myDropdown${f}" class="dropdown-content">
                
                <div style="width:100%">
                <div class="clearfix">
                <div style="width:80%; padding:5px; display:block; float:left;">Total Question</div>
                <div style="width:20%; padding:5px; display:block; float:left;" id=tq${f} ></div>
                </div>
                <div class="clearfix">
                <div style="width:80%; background-color:white; padding:5px; display:block; float:left;">Correct Answer</div>
                <div style="width:20%; background-color:white; padding:5px; display:block; float:left;" id=ca${f} ></div>
                </div>
                <div class="clearfix">
                <div style="width:80%; padding:5px; display:block; float:left;">Incorrect Answer</div>
                <div style="width:20%; padding:5px; display:block; float:left;" id=ica${f} ></div>
                </div>
                <div class="clearfix">
                <div style="width:80%; background-color:white; padding:5px; display:block; float:left;">Un-answered</div>
                <div style="width:20%; background-color:white; padding:5px; display:block; float:left;" id=ua${f}></div>
                </div>
                <div class="clearfix">
                <div style="width:80%; padding:5px; display:block; float:left;">Percent(%)</div>
                <div style="width:20%; padding:5px; display:block; float:left;" id=per${f} ></div>
                </div>
                </div>
                
                
                </div>
            </div></div>` + "</td>");

            $(tr).append("<td>" + `<div class="btn_divv"><button id="removesingle" class="btn2 btn btn-danger glyphicon glyphicon-trash" data-toggle="modal" data-target="#mi-modal"  onclick="remove(${myQuestions[f].id})"></button></div>` + "</td>");

            $(tr).append("<td>" + `<div class="checkboxx"><input  type="checkbox" class="messageCheckbox"  value="${myQuestions[f].id}" id="checkAll"></div>` + "</td>");
            
            $('#table-body').append(tr);
        } 

        delete_selected = function (){

            var checkboxes = document.getElementsByClassName('messageCheckbox');
            var vals = "";
            for (var i=0, n=checkboxes.length;i<n;i++){
                if (checkboxes[i].checked) {
                    vals += ","+checkboxes[i].value;
                }
            }
            if (vals) vals = vals.substring(1);
            localStorage.removeItem("indexuser");
            localStorage.setItem("checkboxesvalues",vals);

            // Eyer checkbox isarelenibse are you sure alertini goster
            if(vals) {
                target = $(".custom").attr('data-target-custom');
                $(target).modal('show');
                    
            }else{
                target2 = $(".custom").attr('data-target-custom2');
                $(target2).modal('show');
            }
    
        };
           

        responsivedrop=function(tq,ca,ica,ua,per, index){

            document.getElementById("tq"+index).innerHTML=tq;
            document.getElementById("ca"+index).innerHTML=ca;
            document.getElementById("ica"+index).innerHTML=ica;
            document.getElementById("ua"+index).innerHTML=ua;
            document.getElementById("per"+index).innerHTML=per;

            document.getElementById("myDropdown"+index).classList.toggle("show");
            window.onclick = function(event) {
                if (!event.target.matches('.drop')) {
                    var dropdowns = document.getElementsByClassName("dropdown-content");
                    var i;
                    for (i = 0; i < dropdowns.length; i++) {
                        var openDropdown = dropdowns[i];
                        if (openDropdown.classList.contains('show')) {
                            openDropdown.classList.remove('show');
                        }
                    }
                }
            }
        }
            
           
        //delete

        remove = function (delete_id){
            if (typeof(Storage) !== "undefined") {
                localStorage.removeItem("checkboxesvalues");
                localStorage.setItem("indexuser", delete_id);
            }
        };

        confirm = function(){
            
            var index=localStorage.getItem("indexuser");
            var ChkbxVal=localStorage.getItem("checkboxesvalues");

            var send_data = {
                index,
                ChkbxVal
            }
            
            xhr = new XMLHttpRequest();
            var url = "http://localhost:3000/deleteuserforuser";
            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-type", "application/json");
            xhr.onreadystatechange = function (){
        
                if (xhr.readyState == 4 && xhr.status == 200){
                    var res = JSON.parse(xhr.responseText);   
                } 
            }
            setTimeout(function () {
                location.reload()
            }, 200);
            
            xhr.send(JSON.stringify(send_data));
        }
    };
});
