$(document).ready(function(){
    $("#myInput").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#table-body tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
});

// Login name displaying

var as= localStorage.getItem("username");
var ln= localStorage.getItem("lastname");
var lc=as[0].toUpperCase();
var al=as.slice(1);

document.getElementById("addd").innerHTML=as+" "+ln;


// check all function
$("#checkAll2").click(function () {
    $('input:checkbox').not(this).prop('checked', this.checked);
});
              

// Header Scrolling
window.onscroll = function() {scrollFunction()};
              
function scrollFunction() {
    if (document.body.scrollTop > 15 || document.documentElement.scrollTop > 15) {
        document.getElementById("navbar").style.padding = "40px 10px";
        document.getElementById("logo").style.fontSize = "25px";
    }else {
        document.getElementById("navbar").style.padding = "60px 10px";
        document.getElementById("logo").style.fontSize = "35px";
    }
}

// Logout Localstorage removing
function logout(){
    localStorage.removeItem("token");
    localStorage.removeItem("username");
    localStorage.removeItem("lastname");
    localStorage.removeItem("email");

    setTimeout(function () {
        location.reload()
    }, 10);
}

// Displayin dropdown pages
var cc=localStorage.getItem("token"); 
if(cc){
    document.getElementById("mdname").style.padding="0px";
    document.getElementById("mdname").innerHTML = `<div  style="margin-bottom:-3px;" class="dropdown">
    <div class="dropdown-toggle glyphicon glyphicon-user" style="padding:15.5px 15.5px 18.5px 15.5px;" data-toggle="dropdown">
        <span style="margin-left:-10px;" class="caret"></span></div>
        <ul class="dropdown-menu2">
        <li><a style="width:193px; text-align:unset;" href="http://localhost:3000/infouser"><span  class="glyphicon glyphicon-user" style="margin-right:5px;"></span><span id="drop_userinfo"></span></a></li>
        <li><a style="width:193px; text-align:unset;" href="http://localhost:3000/changepassword"><span  class="glyphicon glyphicon-lock" style="margin-right:5px;"></span><span id="drop_changepsw"></span></a></li>
        <li><a style="width:193px; text-align:unset;"href="http://localhost:3000/userresult"><span  class="glyphicon glyphicon-ok" style="margin-right:5px;"></span><span id="drop_yourresult"></span></a></li>
        <li><a style="width:193px; text-align:unset;"href="http://localhost:3000/uploadfile"><span  class="glyphicon glyphicon-list-alt" style="margin-right:5px;"></span><span id="drop_sendquestion"></span></a></li>
        <li class="divider2"></li>
        <li><a onclick="logout()" style="width:193px; text-align:unset;"href="#"><span  class="	glyphicon glyphicon-log-out" style="margin-right:5px;"></span><span id="drop_logout"></span></a></li>
        </ul>
    </div>`;
}

// Language Selecting
function select(){
    var lang=document.getElementById("sel1").value;
    localStorage.setItem("lang",lang);
    setTimeout(function () {
        location.reload()
    }, 10);
}


// Getting language datas from database
var resultlangdata=[];
function show(){
    var lang;
    if(localStorage.getItem("lang")==null){
        lang="az"
    }else if (localStorage.getItem("lang")=="az"||"en"){
        lang=localStorage.getItem("lang")
        document.getElementById("sel1").value=lang;
    }

    var language={
        "lang":lang,
        "page":"dropPage4"
    }

    xhr = new XMLHttpRequest();
    var url = "http://localhost:3000/getlanguage";
    var aar = this;
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = function (){

        if (xhr.readyState == 4 && xhr.status == 200){

            var goster = JSON.parse(xhr.responseText); 
            aar.diller(goster);
            resultlangdata.push(goster);
        }
    }
    
    xhr.send(JSON.stringify(language,));
                    
}
show();


function diller(lang){
                          
    for (var key in lang) {
        if (lang.hasOwnProperty(key)) {
            document.getElementById(key).innerHTML=lang[key];
            document.getElementById(key).placeholder=lang[key];
        }
    }
    
}
