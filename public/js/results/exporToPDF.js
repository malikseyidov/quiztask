function Export() {
    $('#tblData').find('th:last-child, td:last-child').remove();
    $('#tblData').find('th:last-child, td:last-child').remove();
    html2canvas(document.getElementById('tblData'), {
        onrendered: function (canvas) {
            var data = canvas.toDataURL();
            var docDefinition = {
                content: [{
                    image: data,
                    width: 500
                }]
            };
            pdfMake.createPdf(docDefinition).download("Result_of_users.pdf");
        }
    });
    
    setTimeout(function () {
        location.reload()
    }, 1);
}