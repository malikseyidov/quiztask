var head = document.getElementsByTagName('head')[0];
var js = document.createElement("script");

var recap=localStorage.getItem("lang");
js.type = "text/javascript";

js.src = "https://www.google.com/recaptcha/api.js?hl=az";

if (recap==null || recap=="az")
{
    js.src = "https://www.google.com/recaptcha/api.js?hl=az";
}
else
{
    js.src = "https://www.google.com/recaptcha/api.js?hl=en";
}

head.appendChild(js);


// blocking the user

var socket = io.connect('http://localhost:3000');
var tokenblocked = localStorage.getItem("token");

socket.on('news', function (data) {

    socket.emit('my other event', tokenblocked );
    if(data.hello[0].blockedno==1){
        localStorage.removeItem("token");
    }
});