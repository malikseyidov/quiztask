function sgnup(){ 
    var name= document.getElementById("signupname").value;
    var lname= document.getElementById("signuplname").value;
    var email = document.getElementById("emailsgnup").value;
    var password = document.getElementById("pswsgnup").value;
    var passwordcn = document.getElementById("pswsgnupconfirm").value;
     
    var logindata = {
        "date": new Date().toString().substr(3,18),
        "username": name,
        "lname": lname,
        "email": email,
        "password": password,
        "lang": localStorage.getItem("lang"),
        "grecaptcharesponse":grecaptcha.getResponse(),
        "blockedno":"0"
    };
     
    xhr = new XMLHttpRequest();
    var url = "http://localhost:3000/signup";
    var that = this;
    xhr.open("POST", url, true);
    
    xhr.setRequestHeader("Content-type", "application/json");
    
    xhr.onreadystatechange = function (){

        if (xhr.readyState == 4 && xhr.status == 200){
            var qisalt = JSON.parse(xhr.responseText);
            that.openAlert(qisalt);

            if(qisalt.action){
                document.getElementById("dsss2").innerHTML = qisalt.message;   
            }else{
                document.getElementById("dsss").innerHTML = qisalt.message;
            }
        } 
    }
    
    
    if(name==""||lname==""||email==""||password==""||passwordcn==""){
    document.getElementById("error").innerHTML ="* Fill all fillings"
    }else if(!email.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)){
    document.getElementById("error").innerHTML ="* You have entered an invalid email address"
    }else if(password.length<4){
    document.getElementById("error").innerHTML ="* Password must contain at least 4 characters"
    }else if(password != passwordcn){
    document.getElementById("error").innerHTML ="* The specified passwords do not match"
    }
    else{
    document.getElementById("error").innerHTML ="";
    xhr.send(JSON.stringify(logindata,));
    }
}