var head = document.getElementsByTagName('head')[0];
var js = document.createElement("script");
var recap=localStorage.getItem("lang");
js.type = "text/javascript";

js.src = "https://www.google.com/recaptcha/api.js?hl=az";

if (recap==null || recap=="az")
{
    js.src = "https://www.google.com/recaptcha/api.js?hl=az";
}
else
{
    js.src = "https://www.google.com/recaptcha/api.js?hl=en";
}

head.appendChild(js);