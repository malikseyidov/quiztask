-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 02, 2019 at 08:21 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quiz`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `blockedNo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `date`, `username`, `lastname`, `email`, `password`, `token`, `blockedNo`) VALUES
(1, ' Mar 05 2019 15:04', 'Malik', 'Seyidov', 'malik@gmail.com', '$2b$10$IlSfRwr3xc3QS3LFyOdc8.Mpg4D0eeDRrSBZ4Q3kcq9d8WWbqpG5G', '$2b$10$IlSfRwr3xc3QS3LFyOdc8.RY8IMBxTr5odmMU9TxpWE/j2YvmN4/a', '0');

-- --------------------------------------------------------

--
-- Table structure for table `modify_accounts`
--

CREATE TABLE `modify_accounts` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modify_accounts`
--

INSERT INTO `modify_accounts` (`id`, `username`, `email`, `password`, `token`) VALUES
(4, 'malik', 'malik@gmail.com', '$2b$10$6KFTDVXvva525n/.cMJCaOn7IyxDdNe4IMNBFnVhMDwQD2Xvus.zW', '$2b$10$6KFTDVXvva525n/.cMJCaONC4UHkMGPp.3xoAIXTgU8OA1JFOsuk.');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `correctAnswer` varchar(30) DEFAULT NULL,
  `aOption` varchar(255) DEFAULT NULL,
  `bOption` varchar(255) DEFAULT NULL,
  `cOption` varchar(255) DEFAULT NULL,
  `dOption` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `date`, `question`, `correctAnswer`, `aOption`, `bOption`, `cOption`, `dOption`) VALUES
(15, ' Mar 16 2019 21:14', 'Amob nece huceyrelidir?', 'a', '1', '2', 'cox huceyreli', 'koloni'),
(16, ' Mar 16 2019 21:45', 'Azerbaycanin paytaxti haradi?', 'a', 'baki', 'gence', 'sumgayit', 'seki');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `id` int(11) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `numberOfQuestion` varchar(255) DEFAULT NULL,
  `numberOfCorrect` varchar(255) DEFAULT NULL,
  `numberOfIncorrect` varchar(255) DEFAULT NULL,
  `numberOfUnanswered` varchar(255) DEFAULT NULL,
  `percentage` varchar(255) DEFAULT NULL,
  `deletedNo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `id` int(11) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `words`
--

CREATE TABLE `words` (
  `id` int(11) NOT NULL,
  `page` varchar(255) DEFAULT NULL,
  `langID` varchar(255) DEFAULT NULL,
  `langKey` varchar(255) DEFAULT NULL,
  `langValue` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `words`
--

INSERT INTO `words` (`id`, `page`, `langID`, `langKey`, `langValue`) VALUES
(1, 'homePage', '1', 'title', 'Ana Səhifə'),
(2, 'homePage', '2', 'title', 'Home'),
(3, 'homePage', '1', 'login', 'Daxil ol'),
(4, 'homePage', '2', 'login', 'Login'),
(21, 'homePage', '1', 'submit', 'Nəticə'),
(22, 'homePage', '2', 'submit', 'Result'),
(23, 'homePage', '1', 'modal_result_note', '* Zəhmət olmasa aşağıdaki boşluqları doldurun!'),
(24, 'homePage', '2', 'modal_result_note', '* Please fill in following blanks!'),
(25, 'homePage', '1', 'nameuser', 'Adınız'),
(26, 'homePage', '2', 'nameuser', 'First Name'),
(27, 'homePage', '1', 'surnameuser', 'Soyadınız'),
(28, 'homePage', '2', 'surnameuser', 'Last Name'),
(29, 'homePage', '1', 'emailuser', 'Emailiniz'),
(30, 'homePage', '2', 'emailuser', 'Email'),
(31, 'homePage', '1', 'show_result', 'Dəvam edin'),
(32, 'homePage', '2', 'show_result', 'Continue'),
(47, 'homePage', '1', 'namepp', 'Hörmətli'),
(48, 'homePage', '2', 'namepp', 'Dear'),
(49, 'homePage', '1', 'emailpp', 'Nəticəniz qeyd etdiyiniz emailə göndərildi'),
(50, 'homePage', '2', 'emailpp', 'Your result has been sent to your email'),
(51, 'homePage', '1', 'x1', 'Sizin'),
(52, 'homePage', '2', 'x1', 'You have got'),
(53, 'homePage', '1', 'x2', 'doğru'),
(54, 'homePage', '2', 'x2', 'correct'),
(55, 'homePage', '1', 'x3', 'yanlış və'),
(56, 'homePage', '2', 'x3', 'incorrect and'),
(57, 'homePage', '1', 'x4', 'cavabsız nəticəniz var. Ümumu sualın sayı'),
(58, 'homePage', '2', 'x4', 'unanswered out of'),
(64, 'homePage', '1', 'x5', '. Müvəffəqiyyətiniz'),
(65, 'homePage', '2', 'x5', 'questions. That is a'),
(66, 'signIn', '1', 'signin_label', 'Daxil ol'),
(67, 'signIn', '2', 'signin_label', 'Sign in'),
(68, 'signIn', '1', 'btn11', 'Daxil ol'),
(69, 'signIn', '2', 'btn11', 'Sign in'),
(70, 'signIn', '1', 'showpsd_check', 'Şifrəni göstər'),
(71, 'signIn', '2', 'showpsd_check', 'Show Password'),
(72, 'SignIn', '1', 'psw', 'Şifrə'),
(73, 'SignIn', '2', 'psw', 'Password'),
(74, 'signIn', '1', 'create_account', 'Hesab yaradın'),
(75, 'signIn', '2', 'create_account', 'Create account'),
(76, 'signIn', '1', 'go_home_page_sgnin', 'Ana səhifə'),
(77, 'signIn', '2', 'go_home_page_sgnin', 'Go Homepage'),
(78, 'signUp', '1', 'signup_label', 'Quizland hesabı yaradın'),
(79, 'signUp', '2', 'signup_label', 'Create your Quizland account'),
(82, 'signUp', '1', 'signupname', 'Adınız'),
(83, 'signUp', '2', 'signupname', 'First Name'),
(84, 'signUp', '1', 'signuplname', 'Soyadınız'),
(85, 'signUp', '2', 'signuplname', 'Last Name'),
(86, 'signUp', '1', 'emailsgnup', 'Emailiniz'),
(87, 'signUp', '2', 'emailsgnup', 'Email'),
(88, 'signUp', '1', 'pswsgnup', 'Şifrə'),
(89, 'signUp', '2', 'pswsgnup', 'Password'),
(90, 'signUp', '1', 'pswsgnupconfirm', 'Şifrənin təkrarı'),
(91, 'signUp', '2', 'pswsgnupconfirm', 'Retype password'),
(92, 'signUp', '1', 'showpswsignup', 'Şifrəni göstər'),
(93, 'signUp', '2', 'showpswsignup', 'Show password'),
(94, 'signUp', '1', 'btn12', 'Qeydiyyat'),
(95, 'signUp', '2', 'btn12', 'Sign Up'),
(96, 'signUp', '1', 'signininstead', 'Hesabınıza daxil olun'),
(97, 'signUp', '2', 'signininstead', 'Sign in instead'),
(98, 'signUp', '1', 'gohomepage', 'Ana səhifə'),
(99, 'signUp', '2', 'gohomepage', 'Go Homepage'),
(100, 'homePage', '1', 'drop_userinfo', 'Haqqınızda'),
(101, 'homePage', '2', 'drop_userinfo', 'User information'),
(102, 'homePage', '1', 'drop_changepsw', 'Şifrəni dəyişin'),
(103, 'homePage', '2', 'drop_changepsw', 'Change password'),
(104, 'homePage', '1', 'drop_yourresult', 'Nəticəniz'),
(105, 'homePage', '2', 'drop_yourresult', 'Your results'),
(106, 'homePage', '1', 'drop_sendquestion', 'Sual göndərin'),
(107, 'homePage', '2', 'drop_sendquestion', 'Send question'),
(108, 'homePage', '1', 'drop_logout', 'Çıxış'),
(109, 'homePage', '2', 'drop_logout', 'Logout'),
(110, 'dropPage', '1', 'droppage_userinformation', 'Sizin məlumatlarınız'),
(111, 'dropPage', '2', 'droppage_userinformation', 'Your informations'),
(112, 'dropPage', '1', 'userfn', 'Adınız'),
(113, 'dropPage', '2', 'userfn', 'First name'),
(114, 'dropPage', '1', 'userln', 'Soyadınız'),
(115, 'dropPage', '2', 'userln', 'Last name'),
(116, 'dropPage', '1', 'userem', 'Emailiniz'),
(117, 'dropPage', '2', 'userem', 'Email'),
(118, 'dropPage', '1', 'ui_gohome', 'Ana səhifə'),
(119, 'dropPage', '2', 'ui_gohome', 'Go Homepage'),
(120, 'dropPage2', '1', 'changepsw', 'Şifrənizi dəyişin'),
(121, 'dropPage2', '2', 'changepsw', 'Change your password'),
(122, 'dropPage2', '1', 'oldpsw', 'Köhnə şifrə'),
(123, 'dropPage2', '2', 'oldpsw', 'Old password'),
(124, 'dropPage2', '1', 'newpsw', 'Yeni şifrə'),
(125, 'dropPage2', '2', 'newpsw', 'New password'),
(126, 'dropPage2', '1', 'confirmnewpsw', 'Yeni şifrəni təsdiqləyin'),
(127, 'dropPage2', '2', 'confirmnewpsw', 'Confirm the new password'),
(128, 'dropPage2', '1', 'showpsw', 'Şifrəni göstər'),
(129, 'dropPage2', '2', 'showpsw', 'Show password'),
(130, 'dropPage2', '1', 'save', 'Yadda saxla'),
(131, 'dropPage2', '2', 'save', 'Save'),
(132, 'dropPage2', '1', 'homepage', 'Ana səhifə'),
(133, 'dropPage2', '2', 'homepage', 'Go Homepage'),
(134, 'dropPage3', '1', 'uploadinginfo', 'Sual faylarının yüklənməsi'),
(135, 'dropPage3', '2', 'uploadinginfo', 'Uploading question files'),
(136, 'dropPage3', '1', 'whichfiles', 'Suallar ancaq PDF və Word halında qəbul olunur. Zəhmət olmazsa sualları cavabları ilə birlikdə göndərin.'),
(137, 'dropPage3', '2', 'whichfiles', 'Tests are only accepted in PDF and Word. Please send tests with correct answers.'),
(138, 'dropPage3', '1', 'download', 'Nümunə'),
(139, 'dropPage3', '2', 'download', 'For example'),
(140, 'dropPage3', '1', 'uploadbutton', 'Yüklə'),
(141, 'dropPage3', '2', 'uploadbutton', 'Upload'),
(142, 'dropPage3', '1', 'gohomeupload', 'Ana səhifə'),
(143, 'dropPage3', '2', 'gohomeupload', 'Go Homepage'),
(144, 'dropPage4', '1', 'userresult_home', 'Ana səhifə'),
(145, 'dropPage4', '2', 'userresult_home', 'Home'),
(146, 'dropPage4', '1', 'userresult_export', 'Eksport'),
(147, 'dropPage4', '2', 'userresult_export', 'Export'),
(148, 'dropPage4', '1', 'myInput', 'Axtar..'),
(149, 'dropPage4', '2', 'myInput', 'Search..'),
(150, 'dropPage4', '1', 'yourresults', 'NƏTİCƏNİZ'),
(151, 'dropPage4', '2', 'yourresults', 'YOUR RESULTS'),
(152, 'dropPage4', '1', 'tabledate', 'Tarix'),
(153, 'dropPage4', '2', 'tabledate', 'Date'),
(154, 'dropPage4', '1', 'tablefn', 'Ad'),
(155, 'dropPage4', '2', 'tablefn', 'First Name'),
(156, 'dropPage4', '1', 'tableln', 'Soyad'),
(157, 'dropPage4', '2', 'tableln', 'Last Name'),
(158, 'dropPage4', '1', 'tabletq', 'Toplam sual'),
(159, 'dropPage4', '2', 'tabletq', 'Total question'),
(160, 'dropPage4', '1', 'tableca', 'Doğru cavab'),
(161, 'dropPage4', '2', 'tableca', 'Correct answer'),
(162, 'dropPage4', '1', 'tableica', 'Yanlış cavab'),
(163, 'dropPage4', '2', 'tableica', 'Incorrect answer'),
(164, 'dropPage4', '1', 'tableuna', 'Cavabsız'),
(165, 'dropPage4', '2', 'tableuna', 'Un-answered'),
(166, 'dropPage4', '1', 'tableper', 'Faiz (%)'),
(167, 'dropPage4', '2', 'tableper', 'Percent (%)'),
(168, 'dropPage4', '1', 'tableresults', 'Nəticələr'),
(169, 'dropPage4', '2', 'tableresults', 'Results'),
(170, 'dropPage4', '1', 'tableaction', 'Əməl.'),
(171, 'dropPage4', '2', 'tableaction', 'Action'),
(172, 'dropPage4', '1', 'userresultdelete', 'Sil'),
(173, 'dropPage4', '2', 'userresultdelete', 'Delete'),
(174, 'dropPage4', '1', 'myModalLabel', 'Diqqət!'),
(175, 'dropPage4', '2', 'myModalLabel', 'Are you sure?'),
(176, 'dropPage4', '1', 'deletemessage', 'Bu sətiri silməyə əminsiniz?'),
(177, 'dropPage4', '2', 'deletemessage', 'Would you like to remove this item from the list?'),
(178, 'dropPage4', '1', 'modal-btn-si', 'Dəvam'),
(179, 'dropPage4', '2', 'modal-btn-si', 'OK'),
(180, 'dropPage4', '1', 'modal-btn-no', 'İmtina'),
(181, 'dropPage4', '2', 'modal-btn-no', 'Cancel'),
(182, 'dropPage4', '1', 'myModalLabel2', ' Diqqət!'),
(183, 'dropPage4', '2', 'myModalLabel2', ' Warning!'),
(184, 'dropPage4', '1', 'noboxselected', 'Heçbir qutucuq seçilməyib'),
(185, 'dropPage4', '2', 'noboxselected', 'No box selected'),
(186, 'dropPage4', '1', 'modal-btn-no2', 'Dəvam'),
(187, 'dropPage4', '2', 'modal-btn-no2', 'OK'),
(188, 'dropPage4', '1', 'drop_userinfo', 'Haqqınızda'),
(189, 'dropPage4', '2', 'drop_userinfo', 'User information'),
(190, 'dropPage4', '1', 'drop_changepsw', 'Şifrəni dəyişin'),
(191, 'dropPage4', '2', 'drop_changepsw', 'Change password'),
(192, 'dropPage4', '1', 'drop_yourresult', 'Nəticəniz'),
(193, 'dropPage4', '2', 'drop_yourresult', 'Your results'),
(194, 'dropPage4', '1', 'drop_sendquestion', 'Sual göndərin'),
(195, 'dropPage4', '2', 'drop_sendquestion', 'Send question'),
(196, 'dropPage4', '1', 'drop_logout', 'Çıxış'),
(197, 'dropPage4', '2', 'drop_logout', 'Logout'),
(204, 'homePage', '1', 'resok', 'Dəvam'),
(205, 'homePage', '2', 'resok', 'OK');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `modify_accounts`
--
ALTER TABLE `modify_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `words`
--
ALTER TABLE `words`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `modify_accounts`
--
ALTER TABLE `modify_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;

--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT for table `words`
--
ALTER TABLE `words`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
