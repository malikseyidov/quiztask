// Module dependencies

var express    = require('express');
var mysql      = require('mysql');
var formidable = require('formidable');
// var session = require('express-session');
var bcrypt = require("bcrypt");
var app = express();
var nodemailer = require('nodemailer');
var bodyParser = require('body-parser'); 
var requestt = require('request');
var path = require('path');
var fs = require('fs');

var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(3000);


// Application initialization

var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'malikk',
    password : '2412'
});
    
connection.connect();

// Database setup

connection.query('CREATE DATABASE IF NOT EXISTS quiz CHARACTER SET utf8 COLLATE utf8_general_ci', function (err) {
    if (err) throw err;
    connection.query('USE quiz', function (err) {
        if (err) throw err;
        connection.query('CREATE TABLE IF NOT EXISTS questions('
            + 'id INT NOT NULL AUTO_INCREMENT,'
            + 'PRIMARY KEY(id),'
            + 'date VARCHAR(255),'
            + 'question VARCHAR(255),'
            + 'correctAnswer VARCHAR(30),'
            + 'aOption VARCHAR(255),'
            + 'bOption VARCHAR(255),'
            + 'cOption VARCHAR(255),'
            + 'dOption VARCHAR(255)'
            +  ')', function (err) {
                if (err) throw err;
            });
            connection.query('CREATE TABLE IF NOT EXISTS accounts('
            + 'id INT NOT NULL AUTO_INCREMENT,'
            + 'PRIMARY KEY(id),'
            + 'date VARCHAR(255),'
            + 'username VARCHAR(255),'
            + 'lastname VARCHAR(255),'
            + 'email VARCHAR(255),'
            + 'UNIQUE(email),'
            + 'password VARCHAR(255),'
            + 'token VARCHAR(255),'
            + 'blockedNo VARCHAR(255)'
            +  ')', function (err) {
                if (err) throw err;
            });
            connection.query('CREATE TABLE IF NOT EXISTS modify_accounts('
            + 'id INT NOT NULL AUTO_INCREMENT,'
            + 'PRIMARY KEY(id),'
            + 'username VARCHAR(255),'
            + 'email VARCHAR(255),'
            + 'UNIQUE(email),'
            + 'password VARCHAR(255),'
            + 'token VARCHAR(255)'
            +  ')', function (err) {
                if (err) throw err;
            });
            connection.query('CREATE TABLE IF NOT EXISTS results('
            + 'id INT NOT NULL AUTO_INCREMENT,'
            + 'PRIMARY KEY(id),'
            + 'date VARCHAR(255),'
            + 'firstname VARCHAR(255),'
            + 'lastname VARCHAR(255),'
            + 'email VARCHAR(255),'
            + 'numberOfQuestion VARCHAR(255),'
            + 'numberOfCorrect VARCHAR(255),'
            + 'numberOfIncorrect VARCHAR(255),'
            + 'numberOfUnanswered VARCHAR(255),'
            + 'percentage VARCHAR(255),'
            + 'deletedNo VARCHAR(255)'
            +  ')', function (err) {
                if (err) throw err;
            });
            connection.query('CREATE TABLE IF NOT EXISTS uploads('
            + 'id INT NOT NULL AUTO_INCREMENT,'
            + 'PRIMARY KEY(id),'
            + 'date VARCHAR(255),'
            + 'filename VARCHAR(255),'
            + 'type VARCHAR(255),'
            + 'size VARCHAR(255),'
            + 'path VARCHAR(255),'
            + 'name VARCHAR(255),'
            + 'lastname VARCHAR(255),'
            + 'email VARCHAR(255)'
            +  ')', function (err) {
                if (err) throw err;
            });
            connection.query('CREATE TABLE IF NOT EXISTS words('
            + 'id INT NOT NULL AUTO_INCREMENT,'
            + 'PRIMARY KEY(id),'
            + 'page VARCHAR(255),'
            + 'langID VARCHAR(255),'
            + 'langKey VARCHAR(255),'
            + 'langValue VARCHAR(255)'
            +  ')', function (err) {
                if (err) throw err;
            });
        connection.query("SELECT COUNT(*) AS cnt FROM modify_accounts WHERE email = ? " , 
        'malik@gmail.com' , function(err , data){
            
            if(err){
                throw(err);
            }  
            else{
                if(data[0].cnt > 0){}
                else{
                                    
                    bcrypt.genSalt(10, function(err, salt) {
                        bcrypt.hash("2412" + "malikseyidov@gmail.com" , salt, function(err, hash) {

                            bcrypt.hash("2412", salt, function(err, hashed_password) {
                                
                                var postVars = {username: "malik", email: "malik@gmail.com", password: hashed_password, token: hash};
                            
                                connection.query("INSERT INTO modify_accounts set ?", postVars, function(err, result) {
                                    
                                if(err) throw err;
                                });
                                
                            }); 
                        });
                    });
                
                } 
            }
            
        })
    });
});


// Configuration

app.use(bodyParser.json());
app.use('/', express.static('public',{extensions: ['htm', 'html']}));


// Show MySQL database

app.post('/show', function (req, res) {

    var lang_num="";
    if(req.body.lang=="az"){
        lang_num="1";
    }else if(req.body.lang=="en"){
        lang_num="2"
    }

    connection.query("SELECT * FROM questions",(err, result) => {
      if(err) { }
        connection.query("SELECT * FROM words where langID="+lang_num+" AND page='"+req.body.page+"' ",(err, result2) => {
            var  test  = {};
            var arr =[];
                
            if(err) {}
            for(var i=0; i<=result2.length-1; i++){
                test[result2[i].langKey] = result2[i].langValue;
            }

            var data=[result,test]
            res.json(data);
        });
    });
});




// Show database for modify

app.post('/showmodify', function (req, res) {

    connection.query("SELECT * FROM questions",(err, result) => {
        if(err) {
            console.log(err); 
            res.json({"error":true});redirect
        }
        else{
            res.json(result);
        }
         
    });
});

// Insert datas to MySQL database

app.post('/add', function (req, res) {
    connection.query("INSERT INTO `questions` (`date`,`question`,`correctAnswer`,`aOption`,`bOption`,`cOption`,`dOption`) VALUES ('"+req.body.date+"','"+req.body.question+"','"+req.body.correctAnswer+"','"+req.body.answers.a+"','"+req.body.answers.b+"','"+req.body.answers.c+"','"+req.body.answers.d+"')", 
    );

    connection.query("SELECT * FROM questions",(err, result) => {
      if(err) {
          console.log(err); 
          res.json({"error":true});
      }
      else { 
          console.log(result); 
          res.json(result); 
      }
  });
});



//Delete

app.post('/delete', function (req, res) {
    if(req.body.index!=null){
        connection.query("DELETE FROM questions  WHERE id = "+req.body.index+" ", function(err, rows){
            connection.query("SELECT * FROM questions",(err, result) => {
                if(err) {
                    console.log(err); 
                    res.json({"error":true});
                }
                else { 
                    console.log(result); 
                    res.json(result); 
                }
            });
        });
    }else if(req.body.ChkbxVal!=null){

        connection.query("DELETE FROM questions  WHERE id IN ("+req.body.ChkbxVal+") ", function(err, rows){
            connection.query("SELECT * FROM questions",(err, result) => {
                if(err) {
                    console.log(err); 
                    res.json({"error":true});
                }
                else { 
                    console.log(result); 
                    res.json(result); 
                }
            });
        });
    }
});



//update

app.post('/update', function (req, res) {
    
    connection.query("UPDATE `questions` SET `date`='"+req.body.date+"',`question`='"+req.body.question+"',`correctAnswer`='"+req.body.correctAnswer+"', `aOption`='"+req.body.answers.a+"', `bOption`='"+req.body.answers.b+"', `cOption`='"+req.body.answers.c+"', `dOption`='"+req.body.answers.d+"' WHERE id = '"+req.body.ind.iddd+"' ", function(err, rows)
    {});
});


// Signup

app.post('/signup',  function(request, response) {
    
    connection.query("SELECT COUNT(*) AS cnt FROM accounts WHERE email = ? " , 
    request.body.email , function(err , data){
        const secretKey = "6Ldc2pAUAAAAAOMgavPsB1Q2Z2dIQD4V2RjWPJE6";
  
    const verificationURL = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + request.body['grecaptcharesponse'] + "&remoteip=" + request.connection.remoteAddress;
       if(err){
           console.log(err);
       }  else if(request.body['grecaptcharesponse'] === undefined || request.body['grecaptcharesponse'] === '' || request.body['grecaptcharesponse'] === null)
       {
        if(request.body.lang=="en"){
            return response.json({action: false, message: "Please select captcha first"});
        }else if(request.body.lang=="az"){
            return response.json({action: false, message: "Zəhmət olmazsa captchanı seçin"});
        }
         
       }  
       else{
           if(data[0].cnt > 0){

                if(request.body.lang=="en"){
                    response.json({action: false, message: "This email already exists!"})
                }else if(request.body.lang=="az"){
                    response.json({action: false, message: "Email halhazırda mövcuddur!"})
                }  
                 
           }else{
            requestt(verificationURL,function(error,res,body) {
                    body = JSON.parse(body);
                
                    if(body.success === undefined || body.success) {
                        bcrypt.genSalt(10, function(err, salt) {
                            bcrypt.hash(request.body.password + request.body.email , salt, function(err, hash) {
                    
                                bcrypt.hash(request.body.password, salt, function(err, hashed_password) {
                                    
                                    var postVars = {date:request.body.date,username: request.body.username, lastname: request.body.lname, email: request.body.email, password: hashed_password, token: hash, blockedNo: request.body.blockedno};
                                
                                    connection.query("INSERT INTO accounts set ?", postVars, function(err, result) {
                                        if(request.body.lang=="en"){
                                            response.json({action: true, message: "You registered successfully!"})
                                        }else if(request.body.lang=="az"){
                                            response.json({action: true, message: "Müvəffəqiyyətlə qeydiyyat oldunuz!"})
                                        }  
                                        
                                    if(err) throw err;
                                    });
                                    
                                }); 
                            });
                        });
                    }
                });
            }    
        }
       
    })
});




  
// Signin

app.post('/signin', function(request, response) {
    const secretKey = "6Ldc2pAUAAAAAOMgavPsB1Q2Z2dIQD4V2RjWPJE6";
    const verificationURL = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + request.body['grecaptcharesponse'] + "&remoteip=" + request.connection.remoteAddress;
    connection.query("SELECT * FROM accounts WHERE email='"+request.body.email_user+"'", function(err, res){
    
        if(res.length!=0){
            data_account = {
                "blockedno" : res[0].blockedNo,
                "username" : res[0].username,
                "lname" : res[0].lastname,
                "email" : res[0].email,
                "token" : res[0].token
            
            }
        
            if(res) {
                
                bcrypt.compare(request.body.password_user, res[0].password, function(err, res) {
                    
                    if(res) {
                        if(request.body['grecaptcharesponse'] === undefined || request.body['grecaptcharesponse'] === '' || request.body['grecaptcharesponse'] === null){
                            if(request.body.lang=="en"){
                                response.json("Please select captcha first");
                            }else if(request.body.lang=="az"){
                                response.json("Zəhmət olmazsa captchanı seçin");
                            }
                            
                        }else
                        {
                            requestt(verificationURL,function(error,ress,body) {
                                body = JSON.parse(body);
                            
                                if(body.success === undefined || body.success&&data_account.blockedno==0) {
                                    response.json(data_account)
                                }else if(data_account.blockedno!=0){
                                    if(request.body.lang=="en"){
                                        response.json("Your account was blocked");
                                    }else if(request.body.lang=="az"){
                                        response.json("Sizin hesabınız bloklanıb");
                                    }
                                }
                                        
                            });
                        }
            
                        
                    }
                    else {
                        if(request.body.lang=="en"){
                            response.json("Email or password is wrong or empty!");
                        }else if(request.body.lang=="az"){
                            response.json("Email vəya şifrəniz yanlış vəya boş buraxılıb!");
                        }
                    }
                        
                    
                });
            }
        
        }
        else{
            
            if(request.body.lang=="en"){
                response.json("Email or password is wrong or empty!");
            }else if(request.body.lang=="az"){
                response.json("Email vəya şifrəniz yanlış vəya boş buraxılıb!");
            }
            
        }      
    });     
});


    // results of Users

app.post('/results', function (req, res) {
    if (req.body.firstName!=""||req.body.lastName!=""||req.body.email!=""){
        connection.query("INSERT INTO `results` (`date`,`firstname`,`lastname`,`email`,`numberOfQuestion`,`numberOfCorrect`,`numberOfIncorrect`,`numberOfUnanswered`,`percentage`,`deletedNo`) VALUES ('"+req.body.date+"','"+req.body.firstName+"','"+req.body.lastName+"','"+req.body.email+"','"+req.body.numberOfQuestion+"','"+req.body.numberOfCorrect+"','"+req.body.numberOfIncorrect+"','"+req.body.numberOfUnanswered+"','"+req.body.percentage+"','"+req.body.deletedno+"')", 
        );
        
        if (!req.body.email.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)){
            connection.query("UPDATE `results` SET `email`='' WHERE email = '"+req.body.email+"' ", function(err, rows)
            {});
        }
        
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            secure: false,
            port: 25,
            auth: {
            user: 'mworkj4@gmail.com',
            pass: 'm4l1k6177'
            }, tls: {
            rejectUnauthorized: false
            }
        });
        
        let mailOptions = {
            from: 'mworkj4@gmail.com',
            to: req.body.email,
            subject: 'Quizland Results',
            text: 'Dear '+req.body.firstName+" "+req.body.lastName+"\n\n"+'You have got '+req.body.numberOfCorrect+' correct '+req.body.numberOfIncorrect+' incorrect and '+req.body.numberOfUnanswered+' unanswered out of '+req.body.numberOfQuestion+' questions.That is '+req.body.percentage+'% success.'+"\n\n"+'Thank you for visiting our website!'
        };
        
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error.message);
            }
            console.log('success');
        });
        
    }

});


// Show Results of Users

app.post('/showresults', function (req, res) {

    connection.query("SELECT * FROM results",(err, result) => {
      if(err) {
          console.log(err); 
          res.json({"error":true});redirect
      }
      else { 
          console.log(result); 
          res.json(result); 
      }
  });
});



//Delete Result of User for User

app.post('/deleteuser', function (req, res) {
    if(req.body.index!=null){
        connection.query("DELETE FROM results  WHERE id = "+req.body.index+" ", function(err, rows)
        {});
    }
    else if(req.body.ChkbxVal!=null){

        connection.query("DELETE FROM results  WHERE id  IN ("+req.body.ChkbxVal+") ", function(err, rows)
        {});

    }
});

// user information

app.post('/changepassword', function (request, response) {

    connection.query("SELECT * FROM accounts WHERE token='"+request.body.token+"'", function(err, res){
            
                   
        if(res.length!=0){
            
            if(res) {
                
                bcrypt.compare(request.body.old_password, res[0].password, function(err, res) {
                   
                    if(res) {
                        
                        bcrypt.genSalt(10, function(err, salt) {
                            
                    
                            bcrypt.hash(request.body.new_password, salt, function(err, hashed_password) {
                                
                                connection.query("UPDATE `accounts` SET `password`='"+hashed_password+"' WHERE token = '"+request.body.token+"' ", function(err, rows)
                                {
                                    response.json({action: true, message: "You changed password successfully!"})
                                    if(err) throw err; 
                                });
                                
                            }); 
                        });
                    }
                    else {
                        response.json({action: false, message: "Password is wrong or empty!"})
                    }
                });
            }
        }
        else{
            response.json({action: false, message: "Token is wrong or empty!"})
        }
    });     
});


// Show individual Results of Users

app.post('/userresult', function (req, res) {

    connection.query("SELECT * FROM results WHERE email='"+req.body.email+"' AND deletedNo='0'", (err, result) => {
      if(err) { 
        res.json({"error":true});redirect
      }
      else { 
        res.json(result); 
      }
    });
});

//Delete Result of User

app.post('/deleteuserforuser', function (req, res) {
    if(req.body.index!=null){
        connection.query("UPDATE `results` SET `deletedNo`='1' WHERE id = '"+req.body.index+"' ", function(err, rows){});
    }
    else if(req.body.ChkbxVal!=null){

        connection.query("UPDATE `results` SET `deletedNo`='1' WHERE id IN ("+req.body.ChkbxVal+") ", function(err, rows)
        {});
    }
});

app.post('/modify_signin', function (request, response) {
 
    const secretKey = "6Ldc2pAUAAAAAOMgavPsB1Q2Z2dIQD4V2RjWPJE6";
              
    const verificationURL = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + request.body['grecaptcharesponse'] + "&remoteip=" + request.connection.remoteAddress;

    
    connection.query("SELECT * FROM modify_accounts WHERE email='"+request.body.email_user+"'", function(err, res){
    
           
        if(res.length!=0){
            data_account = {
                "username" : res[0].username,
                "email" : res[0].email,
                "token" : res[0].token
            
            }
        
            if(res) {
                
                bcrypt.compare(request.body.password_user, res[0].password, function(err, res) {
                   
                    if(res) {
                        if(request.body['grecaptcharesponse'] === undefined || request.body['grecaptcharesponse'] === '' || request.body['grecaptcharesponse'] === null)
                        {
                        response.json("Please select captcha first");
                        } else{
                            requestt(verificationURL,function(error,ress,body) {
                                body = JSON.parse(body);
                            
                                if(body.success === undefined || body.success) {
                                }
                                response.json(data_account)      
                                });
                            ;
                        }
        
                    }
                    else {
                        response.json("Email or password is wrong or empty!");
                    } 
                });
            }
        }
        else{
            
            response.json("Email or password is wrong or empty!");
        }
    });     
});


app.post('/upload', function (req, resp){
    
    app.post('/uploadfile',function(request,response){
        console.log(req.body);
        
        connection.query("SELECT * FROM accounts WHERE email='"+req.body.email+"'", function(err, res){
        
            
            if(res.length!=0){
                data_account = {
                    "token" : res[0].token
                
                }
                
            
                if(res[0].token == req.body.token) {

                    var form = new formidable.IncomingForm();
                    form.parse(request);
                    form.on('fileBegin', function (name, file){
                        file.path = __dirname + '/public/uploads/'+Date.now()+"."+file.name.split('.')[file.name.split('.').length-1];
                        response.send("You uploaded the file successfully!");
                    });
                
                    form.on('file', function (name, file){
                        console.log('Uploaded ' + file.name);
                        console.log(file);
                        console.log(req.body);

                        connection.query("INSERT INTO `uploads` (`date`,`filename`,`type`,`size`,`path`,`name`,`lastname`,`email`) VALUES ('"+req.body.date+"','"+file.name+"','"+file.type+"','"+file.size+"','"+file.path+"','"+req.body.username+"','"+req.body.lname+"','"+req.body.email+"')", );
                            
                            
                    });
                    
                    
                }
            
            }     
        });     
    });
});


// Show users list of  for modify

app.post('/alluserslists', function (req, res) {

    connection.query("SELECT * FROM accounts",(err, result) => {
      if(err) {
          console.log(err); 
          res.json({"error":true});redirect
      }
      else { 
          console.log(result); 
          res.json(result); 
      }
  });
});

// language pages

app.post('/homepage', function (req, res) {
    
    
    connection.query("SELECT * FROM words",(err, result) => {
      if(err) {
          console.log(err); 
          res.json({"error":true});redirect
      }
      else{
          res.json(result);
      }
    });
});

// getlanguage

app.post('/getlanguage', function (req, res) {
    console.log(req.body);
    var lang_num="";
    if(req.body.lang=="az"){
    lang_num="1";
    }else if(req.body.lang=="en"){
    lang_num="2"
    }
    
       
         
    connection.query("SELECT * FROM words where langID="+lang_num+" AND page='"+req.body.page+"' ",(err, result) => {
        var  test  = {};
        
        if(err) {throw(err)}
        for(var i=0; i<=result.length-1; i++){
            test[result[i].langKey] = result[i].langValue;
        }
        res.json(test);   
    });
});
